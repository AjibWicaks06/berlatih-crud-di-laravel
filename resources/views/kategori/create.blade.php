@extends('layout.master')
@section('judul')
    Tambah Cast
@endsection
@section('content')
    
        <form action="/cast" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama Cast</label>
                <input type="text" class="form-control" name="name" placeholder="Masukkan Nama Cast">
                @error('name')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur Cast</label>
                <textarea name="keterangan" class="form-control"></textarea>
                @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur Cast</label>
                <textarea name="keterangan" class="form-control"></textarea>
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>

@endsection