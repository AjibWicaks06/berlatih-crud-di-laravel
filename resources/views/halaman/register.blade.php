<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>

    <form action="/kirim" method="POST">
        @csrf
        <p>First Name</p>
        <input type="text" name="namadepan">
        <p>Last Name</p>
        <input type="text" name="namabelakang">
        <label for="gender">Gender</label>
        <br><br>
        <input type="radio" id="male"><label for="male">Male</label>
        <input type="radio" id="female"><label for="female">Female</label>
        <input type="radio" id="other"><label for="other">Other</label>
        <label for="nasionality">Nationality</label>
        <br><br>
        <select name="" id="nasionality">
            <option value="">Indonesia</option>
            <option value="">Negara Lain</option>
        </select>
        <label for="">Language Spoken</label>
        <br><br>
        <input type="checkbox" name="" id="indonesia"><label for="indonesia">Bahasa Indonesia</label>
        <br>
        <input type="checkbox" name="" id="english"><label for="english">English</label>
        <br>
        <input type="checkbox" name="" id="other"><label for="other">Other</label>
        <br><br>
        <label for="bio">Bio</label>
        <br><br>
        <textarea name="" id="bio" cols="30" rows="10"></textarea>
        <input type="submit"  value="Kirim">
    </form>
</body>
</html>