<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
        $namadepan = $request['namadepan'];
        $namabelakang = $request['namabelakang'];


        return view('halaman.welcome', compact('namadepan', 'namabelakang'));
    }
}
