<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', 'HomeController@home');

Route::get('/register', 'RegisterController@register');

Route::post('/kirim', 'RegisterController@kirim');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/table', function(){
    return view('template.table');
});


Route::get('/data-table', function(){
    return view('template.data-table');
});

//CRUD
Route:: get('/cast/create', 'CastController@create');
Route:: get('/cast', 'CastController@index');
Route:: post('/cast', 'CastController@store');
Route:: get('/cast/{cast_id}', 'CastController@show');
Route:: get('/cast/{cast_id}/edit', 'CastController@edit');
Route:: put('/cast/{cast_id}', 'CastController@update');